#Testing functions/methods for checking headers for rating, time control, and bot presence.

import chess.pgn

#Takes game and checks headers to see if there's a player in the 1600 rating class

def elo1600(x):
	y = x.headers["WhiteElo"]
	z = x.headers["BlackElo"]
	int_y = int(y)
	int_z = int(z)
	if 1600 <= int_y <= 1699:
		print(1)
	elif 1600 <= int_z <= 1699:
		print(1)
	else:
		print(0)
			
#Takes game and returns a list with two elements, the first is white's elo as an int, the second black's.			
			
def int_elo(x):
	y = x.headers["WhiteElo"]
	z = x.headers["BlackElo"]
	int_y = int(y)
	int_z = int(z)
	list = [int_y, int_z]
	return list

#Takes game and checks if time control is 300+0

def clock300_0(x):
	y = x.headers["TimeControl"]
	if '300+0' == y:
		print(1)
	else:
		print(0)
	
#Takes a game and checks if at least one player is a bot
#Can exchange 'BOT' for any other title (e.g. 'FM', 'GM') for same result

def botp(x):
	if 'WhiteTitle' in x.headers:
		if 'BOT' == x.headers["WhiteTitle"]:
			return True
	elif 'BlackTitle' in x.headers:
		if 'BOT' == x.headers["BlackTitle"]:
			return True
	else:
		return False
	
pgn = open("C:\\Users\\User 1\\Python\\test.pgn")
game1 = chess.pgn.read_game(pgn)

elo1600(game1)

print(int_elo(game1))
