#Adaptation of batchtotensor which takes as input a FEN as a string and outputs a 8x8x17 tensor.
#Might want to adapt this to take batches of FEN's and output a batch of 8x8x17 tensors.
#Includes fentt2 function that ignores en passant, as it may not be needed for the second FEN in the state transition.

import numpy as np

def fentt(fen):
	pieces_dict = {'P':0, 'N':1, 'B':2, 'R':3, 'Q':4, 'K':5, 'p':6, 'n':7, 'b':8, 'r':9, 'q':10, 'k':11}
	ep_dict = {'a':0, 'b':1, 'c':2, 'd':3, 'e':4, 'f':5, 'g':6, 'h':7}
	valid_spaces = set(range(1,9))
	board_tensor = np.zeros((8,8,17)) 
	
	fenS = fen.split()
	rown = 0
	coln = 0
	for i, c in enumerate(fenS[0]):
		if c in pieces_dict:
			board_tensor[rown, coln, pieces_dict[c]] = 1
			coln += 1
		elif c == '/':
			rown += 1
			coln = 0
		elif int(c) in valid_spaces:
			coln = coln + int(c)
		else:
			raise ValueError("invalid fensr at index: {} char: {}".format(i,c))
	
	if 'K' in fenS[2]:
		for i in range(8):
			for j in range(8):
				board_tensor[i,j,13] = 1
	
	if 'Q' in fenS[2]:
		for i in range(8):
			for j in range(8):
				board_tensor[i,j,14] = 1
				
	if 'k' in fenS[2]:
		for i in range(8):
			for j in range(8):
				board_tensor[i,j,15] = 1
	
	if 'q' in fenS[2]:
		for i in range(8):
			for j in range(8):
				board_tensor[i,j,16] = 1
				
	if '-' != fenS[3]:
		board_tensor[ep_dict[fenS[3][0]],int(fenS[3][1]) - 1,12] = 1
		
	return board_tensor
	
#The following FEN to tensor function excludes the plane which indicates the en passant square
	
def fentt2(fen):
	pieces_dict = {'P':0, 'N':1, 'B':2, 'R':3, 'Q':4, 'K':5, 'p':6, 'n':7, 'b':8, 'r':9, 'q':10, 'k':11}
	valid_spaces = set(range(1,9))
	board_tensor = np.zeros((8,8,16)) 
	
	fenS = fen.split()
	rown = 0
	coln = 0
	for i, c in enumerate(fenS[0]):
		if c in pieces_dict:
			board_tensor[rown, coln, pieces_dict[c]] = 1
			coln += 1
		elif c == '/':
			rown += 1
			coln = 0
		elif int(c) in valid_spaces:
			coln = coln + int(c)
		else:
			raise ValueError("invalid fensr at index: {} char: {}".format(i,c))
	
	if 'K' in fenS[2]:
		for i in range(8):
			for j in range(8):
				board_tensor[i,j,12] = 1
	
	if 'Q' in fenS[2]:
		for i in range(8):
			for j in range(8):
				board_tensor[i,j,13] = 1
				
	if 'k' in fenS[2]:
		for i in range(8):
			for j in range(8):
				board_tensor[i,j,14] = 1
	
	if 'q' in fenS[2]:
		for i in range(8):
			for j in range(8):
				board_tensor[i,j,15] = 1
		
	return board_tensor
	
