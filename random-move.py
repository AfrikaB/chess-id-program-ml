#Takes as input a game pgn and outputs two FENs corresponding to a random move in the game.
#Try to get clock info
#Building the FEN's in this way causes them to lose the halfmove clock and fullmove number, possibly also en passant
#Halfmove clock: This is the number of halfmoves since the last capture or pawn advance. This is used to determine if a draw can be claimed under the fifty-move rule.
#Fullmove number: The number of the full move. It starts at 1, and is incremented after Black's move.

import chess.pgn
import chess
import random

def rmove(x):
	l = [] 
	for move in x.mainline_moves():
		l.append(move)
	r = random.randint(1,len(l))
	a1 = x.board()
	if r == 1:
		a = a1.board_fen()
		a1.push(l[0])
		b = b1.board_fen()
	if r > 1:
		t = 0
		while t < r - 1:
			a1.push(l[t])
			t += 1
		a = a1.board_fen()
		a1.push(l[t])
		b = a1.board_fen()
	z = [a,b,r]
	return z

pgn = open("C:\\Users\\User 1\\Python\\test.pgn")
game1 = chess.pgn.read_game(pgn)

print(rmove(game1))
