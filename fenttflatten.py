#Takes an 8x8x17 FEN to tensor and flattens it, removing the excess 0's and 1's from constant layers to reduce size

import numpy as np

def fenttflatten(tensorinput):
	trim = np.zeros((8,8,13))
	
	for i in range(8):
		for j in range(8):
			for k in range(13):
				trim[i,j,k] = tensorinput[i,j,k]
	
	part1 = trim.flatten()
	part1 = part1.reshape(1,832)
	
	part2 = np.zeros((1,4))
	
	#This could be written a lot more concisely
	
	if tensorinput[0,0,13] == 1:
		castle[0][0] = 1
	else:
		castle[0][0] = 0
	
	if tensorinput[0,0,14] == 1:
		castle[0][1] = 1
	else:
		castle[0][1] = 0
	
	if tensorinput[0,0,15] = 1:
		castle[0][2] = 1
	else:
		castle[0][2] = 0
		
	if tensorinput[0,0,16] = 1:
		castle[0][3] = 1
	else:
		castle[0][3] = 0
		
	flatoutput = np.append(part1,part2)
	flatoutput = flatoutput.reshape(1,836)
	
	return flatoutput

